---
title: Atelier des communs, Documenter et cartographier les communs et les alternatives
---

# Présentation

Vous contribuez à des communs et des alternatives ? Venez les documenter et les cartographier pendant cet atelier, pour les donner à voir sur une carte !

Le thème principal de cet atelier de contribution est de cartographier les lieux auto-gérés, tiers-lieux, lieux pour et par les gens, fablab, hackerspaces, lieux d'artistes, bars associatifs, etc. Mais si vous avez d'autres communs à documenter c'est ouvert aussi !

![Carte Toulouse par Cassini, 1815](https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Toulouse_cassini_1815.tif/lossy-page1-600px-Toulouse_cassini_1815.tif.jpg) 
par Cassini, Domaine public

# Déroulé

## Introduction 5min

Prés: dev web (HTML/CSS, python django) dans une entreprise qui fait de la carto, militante libriste, dans plusieurs assos et mouvement comme les Communs, Alternatiba et Framasoft. Dans le mouvement des Communs, ainsi qu'à Alternatiba, on essaie de documenter les communs et les alternatives.

L'objectif de l'atelier: 

* Initier une cartographie des tiers-lieux à Toulouse, à l'aide d'outils libres : Umap et Communecter, bien sûr en OpenData
* S'essayer à des outils carto sans avoir besoin de connaissances dev

*Disclaimer:* travail en cours, pas encore étudié toute la typologie existante, sûrement pas encore exploré toutes les ressources, si vous en avez je suis preneuse.

*Disclaimer2:* pas géomaticienne, et débutante en dev carto

## C'est quoi un tiers-lieu ?

> Le tiers-lieu, ou la troisième place, est un terme traduit de l'anglais The Third Place faisant référence aux environnements sociaux qui viennent après la maison et le travail. C'est une thèse développée par Ray Oldenburg1, professeur émérite de sociologie urbaine à l’université de Pensacola en Floride, dans son livre publié en 1989 : The Great, Good Place.

*Source: [Wikipedia](https://fr.wikipedia.org/wiki/Tiers-lieu)*

> Le « Tiers Lieux » est une « configuration sociale » qui se matérialise le plus souvent par un « lieu physique et/ou numérique » dans lequel est activé par l’action du « Concierge* » un « processus singulier » qui va permettre à des « personnes venues d’univers différents », voire contradictoires, de se rencontrer, se parler et créer ainsi un « 3ème langage » leur permettant de construire des projets (en) « communs ».

*Source: [Tiers-lieux OpenSource](http://tiers-lieux.org/les-tiers-lieux/)*

## Les ressources existantes

Des cartes thématiques (fablab, coworking, …) et des cartes par territoire, sous plusieurs formes : pdf (!), google maps, Umap, Communecter network, code maison avec bibliothèques de carto web (leaflet par exemple). Les données ne sont pas toutes en OpenData.

Des jeux de données en OpenData.

### Cartes

* [Tiers-lieux open source](http://tiers-lieux.org/carte/) (Communecter)
* [Tiers-lieux de la MEL (Lille)](http://hauts.tiers-lieux.org/mel/) (Communecter)
* [Tiers-lieux en Aquitaine](https://coop.tierslieux.net/le-reseau/carte/) 
* [Wiki sur les tiers-lieux](http://movilab.org/index.php?title=Accueil)
* [Wiki des communs](http://wiki.lescommuns.org/)
* [Tiers-lieux en Occitanie](http://www.tierslieuxoccitanie.com/working_places) carte google maps, pas trouvé les données
* [Tiers-lieux dans le Tarn](http://coworking-tarn.com/#lestierslieux), informations 
* [Tiers-lieux dans le Gers](http://plage.kopane.fr/wakka.php?wiki=LesLieux), données disponibles en CSV ou Json (merci Jeey !)
* [Lieux numérique Pays de Loire](http://www.parcoursnumeriques.net/carte/)
* [Cartes sur fablab.fr](http://www.fablab.fr/les-fablabs/carte-des-fablabs/) - [Carte](https://www.fablabs.io/labs/map)
* [Cartes de tous les labs par Makery](http://www.makery.info/labs-map/)
* [Coworking et tiers-lieux sur RTES](http://rtes.fr/Coworking-et-Tiers-Lieux) et la [carte](http://www.coworking-carte.fr) (google maps)
* [Tiers-lieux en Île et Vilaine](http://www.audiar.org/node/449?#) (pdf)

### Data

* [Coworking et Tiers-lieux sur Data.toulouse-metropole.fr](https://data.toulouse-metropole.fr/explore/dataset/structures-dhebergement-et-daccompagnement-a-destination-dentreprises-coworking-/table/?location=13,43.60977,1.40598&basemap=jawg.streets)

Et en construction [Carte des tiers-lieux et Fablab du Lot et Garonne](http://cci47.fr/Entreprendre-Aquitaine-Midi-Pyrenees/Creation-Reprise-Transmission-d-entreprise/Fablab-et-tiers-lieux/La-carte-des-tiers-lieux-et-Fablab)

## Présentation des outils

### Umap

Avantage: pas de connexion nécessaire, autonomie, import de plusieurs jeux possible simplement en créditant

Inconvénient: évolution des données possible mais pas simple

* Créer une carte
* Ajouter un point sur la carte, personnaliser le 
* Importer des données du portail OpenData Toulouse, par exemple [les espaces de coworking](https://data.toulouse-metropole.fr/explore/dataset/structures-dhebergement-et-daccompagnement-a-destination-dentreprises-coworking-/), dans un nouveau calque
* Personnaliser le calque
* Personnaliser l'affichage global de la carte


<iframe width="100%" height="300px" frameBorder="0" src="https://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=caption&captionBar=false"></iframe>

<p><a href="http://umap.openstreetmap.fr/fr/map/tiers-lieux-toulousains_210845">Voir en plein écran</a></p>


### Communecter

Plateforme web avec nombreuses fonctionalités: media social, cartographie, recherche, petites annonces…

Notamment CO Network, outil permettant d'afficher une carte filtrant sur des mots-clé ou type de contenu (orga, citoyens…), configurable à l'aide d'un fichier json.

Avantage: fonctionnalités suplémentaires, nombreuses données possibles

Inconvénients:

* nécessaire de se connecter pour saisir des informations sur votre lieu.
* import des données possible, mais il faut demander le rôle "admin public" aux administrateurs, pas encore d'API pour créer des objets
* encore du flou pour définir la licence
* pour le moment hébergé chez AWS, mais travaillent à changer ça

Un exemple de représentation des données issues de Communecter filtrées sur l'Occitanie. 

<iframe src="https://www.communecter.org/network/default/index?src=https://framagit.org/numahell/co_networks/raw/master/tierslieux-occitanie.json" width="100%" height="300px" frameBorder="0" ></iframe>

Evidemment, il manque un gros travail d'import des données.

### Les questions

Suggestion sur les bases de données : faire en sorte qu'elles soient décentralisées mais qu'elles puissent communiquer entre elles.

Info: à Bruxelles, un travail similaire est en cours, et discussions ont eu lieu pour mettre en place une base de données des tiers-lieux types hackerspaces.

## Topologie des tiers-lieux

# Préparation (non évoqué pendant l'atelier)

## Récupérer des données

(pas forcément exhaustives)

Des pistes:

* formulaire pour ajouter les tiers-lieux ? Un framaform ? ou bien un ajout et en édition ? 
* Juste un pad ?
* opendata
* autres cartes

## Importer dans communecter

Liste les données nécessaires
motclé commun : tiers-lieux-occitanie ? Quel est celui pour ceux d'Aquitaine ?

## Typologie des tiers-lieux

* publics accueillis
* activités - activité principale ? 
* structure juridique
* gouvernance
* types : bar associatif, hackerspace, fablab, résidence d'artistes
