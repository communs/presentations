---
title: Les communs, le buzzword 2018 ?
revealOptions:
    transition: 'fade'
---

# Les communs, le buzzword 2018 ?

---

# Une définition des communs

----

Basée sur les travaux de [Elinor Ostrom](https://fr.wikipedia.org/wiki/Elinor_Ostrom)

![Elinor Ostrom](./images/689px-Nobel_Prize_2009-Press_Conference_KVA-30-cc-by-sa-holger-motzkau.jpg)

*prix d'Économie de la Banque de Suède en hommage à Nobel en 2009*

----

![Illu](./images/Les_communs-cc-by-sa-pierre-trendel.png)

Note: Un commun = des **personnes** ayant accès à une ressource qui créent collectivement **des règles** pour produire et préserver **des ressources**.

----

* Pas seulement la ressource, aussi les relations sociales autour de cette ressource
* Faisceau de droits (usage, glanage,…) VS droit exclusif de propriété
* Collectif = micro-institution (E. Ostrom dans la lignée de certains économistes institutionnels)

----

## La renaissance des communs

----

* Internet → logiciels libres, Wikipedia, OpenStreetMap…
* Redécouverte de l'anthropologie économique et de l'héritage de Mauss ?

----

## Pas de communs sans commoners

> « Il n'y a pas de commun sans « commoners ». (…) Il n'y a pas de commun sans agir en commun. » 
*New to the Commons ?, David Bollier*

> « Ce que nous construisons en communs »
B. Coriat

Note:

> « […] seule une pratique de mise en commun peut décider de ce qui est « commun », réserver certaines choses à l'usage commun, produire les règles capables d'obliger les hommes. » 
Commun. Essai sur la révolution du XXIème siècle, Pierre Dardot et Christian Laval

----

## État-Marché ?

Ostrom: gestion publique ou privée peut ne pas être aussi efficace qu'une gestion en commun

Une autre voie entre Marché et État (D. Bollier)

---

# Un détracteur influent

----

## Garett Hardin, Tragédie des communs (1968)

> « Imaginez un pâturage ouvert à tous… »

De façon rationnelle, pour maximiser leurs profits, les bergers sont amenés à surexploiter. Donc seule la gestion privée ou publique permet de gérer cette ressource.

----
![](./images/g-nie-des-alpages-le-tome-3-barre-toi-de-mon-herbe-dargaud.jpg)
----

### Elinor Ostrom répond

* Ressource en libre accès, non pas une ressource gérée en commun
* Hardin néglige la communication et les relations de coopération entre les bergers

Les travaux de Ostrom montrent ensuite que gestion publique ou privée souvent moins efficace que gestion en commun

----

### Liens

[The Tragedy of the Commons, Garrett Hardin, 13 Dec 1968](http://science.sciencemag.org/content/162/3859/1243.full)

[La tragédie des communs était un mythe,  04.01.2018, par Fabien Locher
](https://lejournal.cnrs.fr/billets/la-tragedie-des-communs-etait-un-mythe)

---

# Les bullshiteurs récupérateurs

----

## Macron à Davos

* Parle des biens communs ! 
* Parle-t'il de la même chose ?

![](./images/macron-ue-by-gue-ngl-cc-by-nc-nd.jpg)

----

### Tout est bien commun !

* Développement économique durable
* et en même temps éducation et santé
* ou encore accès au numérique 

*(youpi)*

----

### Protéger du passager clandestin

Les biens communs en danger, la faute au passager clandestin qui profite… 

*oui vous, les grandes puissances !*

*(tremblez)*

----

![](./images/affiche-redisdead-lesnuls.jpg)

----

### Comment ?

* Arrêtons les fiscalités trop avantageuses, et taxons les géants de l'internet *(tremblez, bis)*

* Et en même temps, pour le financer, investissez ! vive les partenariats-publics privés *(on est à Davos quand même)*

Note:
Cher Bolloré, pour 1$ investi au Togo, s'il te plaît peut-tu investir 1$ pour l'éducation des jeunes filles ? merci, bisous

----

### Liens

[Son discours](http://www.elysee.fr/declarations/article/transcription-du-discours-du-president-de-la-republique-au-forum-economique-mondial-de-davos-suisse/)

[Les « biens communs » d’Emmanuel Macron ne sont pas les nôtres !, Lionel Maurel, 2/10/2017](https://scinfolex.com/2017/10/02/les-biens-communs-demmanuel-macron-ne-sont-pas-les-notres/)

----

## Économie du bien commun, par Jean Tirole

L'économie des biens communs = juste équilibre entre État et Marché

Son crédo : concurrence au sein d'un marché, régulé de façon bienveillante et efficace par l'État. 

Note: 
Jean-Michel Servet a lu le livre pour nous et n'a pas de mots assez durs :

> « Le manque d’intérêt de l’auteur pour
la problématique montante de leur reconnaissance et de leur
construction à des niveaux locaux et globaux dans les domaines
les plus variés, frappe l’ouvrage, du fait même de son titre,
d’obsolescence scientifique et politique. »

Tirole ne croit qu'en la concurrence au sein d'un marché, régulé de façon bienveillante et efficace par l'État.
L'économie des biens communs n'est pour lui qu'un juste équilibre entre État et Marché, il ignore complètement les modes d'organisation mis à jour par les travaux récents sur les communs.

> « La reconnaissance des
communs pose de façon nouvelle la question des droits d’usage,
de la mutualisation des activités en dépassant celle de la complé-
mentarité et l’opposition des interventions publiques et privées »

Jean-Michel Servet et al., « Économie du bien commun. Plusieurs lectures », La
Revue des Sciences de Gestion 2016/4 (N° 280), p. 101-108.
DOI 10.3917/rsg.280.0101

----

### Liens

[Économie du bien commun](https://www.puf.com/content/%C3%89conomie_du_bien_commun)

[Jean-Michel Servet, « Économie du bien commun. Plusieurs lectures », La
Revue des Sciences de Gestion 2016/4](https://www.cairn.info/revue-des-sciences-de-gestion-2016-4-p-101.htm) - [accès libre](https://www.pauljorion.com/blog/2016/07/29/lavenir-dans-un-retroviseur-une-lecture-de-leconomie-du-bien-commun-de-jean-tirole-par-jean-michel-servet/)

---

# Bref, ils restent à Davos

----

## Bien commun ou service public ?

* ambiguïté entre biens communs et biens publics ou services publics
* l’action publique pour pallier à la « défaillance du marché »
* aucune voie autre que État et Marché 

> « putains de partenariats public-privés » - [Olivier Ertzcheid](http://affordance.typepad.com/), cf [Facebook forme les chômeurs, Google forme les étudiants. Et les universités vous emmerdent.](http://affordance.typepad.com/mon_weblog/2018/02/facebook-google-universite-formation-et-merde.html)

----

## Négation du collectif

![](./images/faineants-unissez-by-menjoulet-cc-by.jpg)

Note: biens communs en tant que grands principes, mais rien sur les groupes qui peuvent les gérer ni sur les modes de gouvernance (coopératives, collectifs, mutualités…)

---

# Et la dimension politique ?

----

## Critique plus constructive

* Elinor Ostrom a étudié surtout des communautés de taille limitée
* N'a pas assez étudié les rapports de pouvoir
* Laisse de côté « l’architecture globale des systèmes sociaux de production »

----

### Lien

[Olivier Weinstein, « Comment comprendre les « communs » : Elinor Ostrom, la propriété et la nouvelle économie institutionnelle  », Revue de la régulation, 14 | 2e semestre / Autumn 2013](http://journals.openedition.org/regulation/10452)

----

## L'organisation en fédérations diverses, une voie à explorer ?

---

# Merci !

----

# Ressources

* [Lescommuns.org](https://lescommuns.org/), portail des communs
* [Wikibook "Construire des communs](https://fr.wikibooks.org/wiki/Construire_des_communs)

En anglais

* [Commontransitions](http://commonstransition.org/)

----

## Le mouvement

* [Remix the commons](https://www.remixthecommons.org/), documenter les pratiques
* [Les communs d'abord](https://les-communs-dabord.org/), la veille des communs
* [Wiki](https://wiki.lescommuns.org/), [forum](https://forum.lescommuns.org/), [chat](https://chat.lescommuns.org/)

---

# Crédits

* [Elinor Ostrom](), CC-By-SA, Holger Motzkau
* [Les communs c'est quoi](), CC-By-SA, Pierre Trendel
* [Génie des Alpages](http://www.dargaud.com/bd/F-MURRR/Genie-des-Alpages-Le), Fmurr', Dargaud
* [Macron au parlement](), CC-By-NC-ND, GUE/NGL
* [Affiche Red is Dead](http://redisdead.timlebon.fr/pages/redisdead.htm)
* [Fan Art Red is Dead](https://dawid-b.deviantart.com/art/Red-is-dead-484293916), Dawid-B
* [Manif](), CC-By, Menjoulet
